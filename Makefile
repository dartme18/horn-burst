DEBUG=true
ifeq "$(DEBUG)" "true"
DEBUGOPTS=-g -O0 -fsanitize=address -fsanitize=undefined -fno-sanitize-recover=all -fsanitize=float-divide-by-zero -fsanitize=float-cast-overflow -fno-sanitize=null -fno-sanitize=alignment
else
DEBUGOPTS=-O3 -DNDEBUG
endif
CC=g++
CXXFLAGS=-Wall -Wextra -pedantic -std=c++17 -fdiagnostics-color $(DEBUGOPTS)
sources=$(wildcard src/*.cpp)
objects=$(sources:src/%.cpp=obj/%.o)
headers=$(wildcard src/*.hpp)
precompiledheader=src/stdafx.h.gch
executable=dist/hornburst
headerdependencies=$(objects:obj/%.o=obj/%.d)
resourcesrc=$(shell find resources/ -type f)
resourcedest=$(resourcesrc:resources/%=dist/%)

$(executable): $(precompiledheader) $(objects) Makefile | obj dist $(resourcedest)
	@echo making $@
	@$(CC) $(CXXFLAGS) -o $(executable) $(objects) -lpthread \
		-lwt -lwthttp -lwtdbo -lwtdbosqlite3

$(resourcedest) : dist/% : resources/% | dist
	mkdir -p $(dir $@);
	cp -r $< $@;

-include $(headerdependencies)

obj:
	if [ ! -e obj ]; then mkdir obj; fi;
dist:
	if [ ! -e dist ]; then mkdir dist; fi;

# Static pattern rules. Take each object in the targets, create a "base"
# with it specified by % (removing the literal part '.o'), then
# use that base to generate the prerequisite. This generates
# several rules.  For instance,  main.o : "main".o : "main".cpp
# then refer to the left-most prerequisite with $<, and the target
# with $@ like usual.
# "|" signals an order-only prerequisite: the prereq must exist, but
# it doesn't matter if it's newer.
$(objects) : obj/%.o : src/%.cpp $(precompiledheader) Makefile | obj
	@echo making $@
	@$(CC) -c $(CXXFLAGS) -MMD -Isrc/spdlog/include $< -o $@

$(precompiledheader) : src/stdafx.hpp | obj
	@echo making $@
	$(CC) $(CXXFLAGS) -Isrc/spdlog/include $< -o $@

clean :
	rm -rf obj dist
