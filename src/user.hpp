#pragma once

using user_ptr = Wt::Dbo::ptr<struct user>;

namespace Wt::Dbo
{
    template<>
    struct dbo_traits<struct user> : public dbo_default_traits
    {
        typedef std::string IdType;
        static IdType invalidId()
        {
            return {};
        }

        static const char * surrogateIdField()
        {
            return nullptr;
        }
    };

    template<>
    struct dbo_traits<const user> : public dbo_traits<user>
    {
    };
}

struct user : public Wt::Dbo::Dbo<user>
{
    user(void) = default;
    /* Wt::Dbo::Dbo doesn't support copies.*/
    user(user const & ) = delete;
    user(user &&) = default;
    user(std::string const & name);

    static user_ptr get_user(std::string const & name, Wt::Dbo::Session &);
    std::string name;

    template<class A>
    void persist(A & a)
    {
        Wt::Dbo::id(a, name, "name");
    }
};
