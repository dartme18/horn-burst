#include "stdafx.hpp"

#include "pieces_widget.hpp"
#include "piece.hpp"

void pieces_widget::add_table_header(Wt::WTable & table, std::string const & text)
{
    int const new_column{table.columnCount()};
    table.elementAt(0, new_column)->addNew<Wt::WText>(text);
    // take text by copy
    table.elementAt(0, new_column)->clicked().connect([this, text]{sort_by(text);});
}

void pieces_widget::show_pieces(std::vector<piece_ptr> && p, search_parameters const & params)
{
    pieces = std::move(p);
    if (params.sort_by.size())
        sort_by(params.sort_by);
    else
        sort_by(parameters.sort_by);
    render();
}

void pieces_widget::add_element(Wt::WTable & table, int row, int column, std::string const & column_name, piece_ptr const & piece)
{
    std::string text;
    if (column_name == "Title")
        text = piece->title;
    if (column_name == "Difficulty")
        text = piece->difficulty;
    if (column_name == "Composer")
        text = piece->composer;
    if (column_name == "Transpositions")
        text = piece->transpositions;
    if (column_name == "Edition")
        text = piece->edition;
    if (column_name == "Duration")
        text = piece->duration;
    if (column_name == "Range")
        text = piece->range;
    if (column_name == "Endurance")
        text = piece->endurance;
    Wt::WTableCell * cell {table.elementAt(row, column)};
    cell->clicked().connect([this, row]{toggle_detail(row);});
    cell->addNew<Wt::WText>(text);
}

void pieces_widget::render(void)
{
    clear();
    table = addNew<Wt::WTable>();
    table->setHeaderCount(1, Wt::Orientation::Horizontal);
    add_table_header(*table, "Title");
    add_table_header(*table, "Difficulty");
    add_table_header(*table, "Composer");
    add_table_header(*table, "Transpositions");
    add_table_header(*table, "Edition");
    add_table_header(*table, "Duration");
    add_table_header(*table, "Range");
    add_table_header(*table, "Endurance");
    for (unsigned i{0}; i < pieces.size(); ++i)
    {
        // offset one to account for the first row being a header row
        add_element(*table, i+1, 0, "Title", pieces[i]);
        add_element(*table, i+1, 1, "Difficulty", pieces[i]);
        add_element(*table, i+1, 2, "Composer", pieces[i]);
        add_element(*table, i+1, 3, "Transpositions", pieces[i]);
        add_element(*table, i+1, 4, "Edition", pieces[i]);
        add_element(*table, i+1, 5, "Duration", pieces[i]);
        add_element(*table, i+1, 6, "Range", pieces[i]);
        add_element(*table, i+1, 7, "Endurance", pieces[i]);
    }
}

void pieces_widget::sort_by(std::string const & text)
{
    if (!text.size())
        return;
    parameters.sort_by = text;
    _search_parameters_requested(parameters);
    std::sort(pieces.begin(), pieces.end(), [this](piece_ptr const & lhs, piece_ptr const & rhs)
        {
            if (strcasecmp(parameters.sort_by.c_str(), "Title") == 0)
                return lhs->title < rhs->title;
            if (strcasecmp(parameters.sort_by.c_str(), "Difficulty") == 0)
                return lhs->difficulty < rhs->difficulty;
            if (strcasecmp(parameters.sort_by.c_str(), "Composer") == 0)
                return lhs->composer < rhs->composer;
            if (strcasecmp(parameters.sort_by.c_str(), "Transpositions") == 0)
                return lhs->transpositions < rhs->transpositions;
            if (strcasecmp(parameters.sort_by.c_str(), "Edition") == 0)
                return lhs->edition < rhs->edition;
            if (strcasecmp(parameters.sort_by.c_str(), "Duration") == 0)
                return lhs->duration < rhs->duration;
            if (strcasecmp(parameters.sort_by.c_str(), "Range") == 0)
                return lhs->range < rhs->range;
            if (strcasecmp(parameters.sort_by.c_str(), "Endurance") == 0)
                return lhs->endurance < rhs->endurance;
            ERROR("Sort by parameter not found");
            return false;
        });
    render();
}

Wt::Signal<search_parameters const &> & pieces_widget::search_parameters_requested(void)
{
    return _search_parameters_requested;
}

void pieces_widget::set_parameters(search_parameters const & params)
{
    sort_by(params.sort_by);
    render();
}

void pieces_widget::toggle_detail(int row)
{
    if (showing_details > -1)
    {
        table->removeRow(showing_details+1);
        INFO("row is {}, showing_details is {}; removing the latter.", row, showing_details);
        if (row == showing_details)
        {
            showing_details = -1;
            return;
        }
    }
    if (!table)
    {
        ERROR("No table to insert show detail!");
        return;
    }
    Wt::WTableRow * new_row {table->insertRow(row+1)};
    Wt::WTableCell * new_cell{new_row->elementAt(0)};
    new_cell->setColumnSpan(8);
    new_cell->addNew<Wt::WText>("Whoa, worked!");
    new_cell->clicked().connect([this, row]{toggle_detail(row);});
    showing_details = row;
}
