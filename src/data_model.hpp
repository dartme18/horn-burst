#pragma once

struct data_model : public Wt::Dbo::Session
{
    data_model(std::unique_ptr<Wt::Dbo::SqlConnection> &&);
    bool add_new_piece(std::string const & title, std::string const & difficulty, std::string const & composer,
            std::string const & transpositions, std::string const & edition, std::string const & duration,
            std::string const & range, std::string const & endurance);
    std::vector<Wt::Dbo::ptr<struct piece>> get_pieces(void);
};
