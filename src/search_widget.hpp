#pragma once
#include "utils.hpp"

struct search_widget : public Wt::WContainerWidget
{
    search_widget(struct data_model &, struct login_service &);
    // When the user does something that will change the search parameters, this
    // is fired so that the internal path can be updated properly.
    Wt::Signal<search_parameters const &> & search_parameters_requested(void);
    void set_parameters(search_parameters const &);

private:
    data_model & model;
    login_service & login;
    struct pieces_widget * results;

    void search(void);
};
