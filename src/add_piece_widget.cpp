#include "stdafx.hpp"

#include "add_piece_widget.hpp"
#include "data_model.hpp"
#include "login_service.hpp"
#include "piece.hpp"

add_piece_widget::add_piece_widget(struct data_model & model, struct login_service & login)
    : model{model}
    , login{login}
{
    auto title_label{this->addNew<Wt::WLabel>("Title:")};
    title = this->addNew<Wt::WLineEdit>();
    title_label->setBuddy(title);
    auto difficulty_label{this->addNew<Wt::WLabel>("Difficulty:")};
    difficulty = this->addNew<Wt::WLineEdit>();
    difficulty_label->setBuddy(difficulty);
    auto composer_label{this->addNew<Wt::WLabel>("Composer:")};
    composer = this->addNew<Wt::WLineEdit>();
    composer_label->setBuddy(composer);
    auto transpositions_label{this->addNew<Wt::WLabel>("Transpositions:")};
    transpositions = this->addNew<Wt::WLineEdit>();
    transpositions_label->setBuddy(transpositions);
    auto edition_label{this->addNew<Wt::WLabel>("Edition:")};
    edition = this->addNew<Wt::WLineEdit>();
    edition_label->setBuddy(edition);
    auto duration_label{this->addNew<Wt::WLabel>("Duration:")};
    duration = this->addNew<Wt::WLineEdit>();
    duration_label->setBuddy(duration);
    auto range_label{this->addNew<Wt::WLabel>("Range:")};
    range = this->addNew<Wt::WLineEdit>();
    range_label->setBuddy(range);
    auto endurance_label{this->addNew<Wt::WLabel>("Endurance:")};
    endurance = this->addNew<Wt::WLineEdit>();
    endurance_label->setBuddy(endurance);
    auto submit{addNew<Wt::WPushButton>("Submit")};
    submit->clicked().connect(this, &add_piece_widget::submit);
}

void add_piece_widget::submit(void)
{
    if (!model.add_new_piece(title->text().toUTF8(), difficulty->text().toUTF8(), composer->text().toUTF8(),
            transpositions->text().toUTF8(), edition->text().toUTF8(), duration->text().toUTF8(),
            range->text().toUTF8(), endurance->text().toUTF8()))
    {
        ERROR("New piece adding failed.");
        addNew<Wt::WLabel>("failed.");
        return;
    }
    addNew<Wt::WLabel>("added!" + title->text().toUTF8());
}
