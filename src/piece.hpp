#pragma once

using piece_ptr = Wt::Dbo::ptr<struct piece>;

struct piece : public Wt::Dbo::Dbo<piece>
{
    piece(void) = default;
    piece(std::string const & title, std::string const & difficulty, std::string const & composer,
            std::string const & transpositions, std::string const & edition,
            std::string const & duration, std::string const & range,
            std::string const & endurance);
    /* Wt::Dbo::Dbo understandably doesn't support copies.*/
    piece(piece const & ) = delete;
    piece(piece &&) = default;
    piece(std::string const & name);

    static Wt::Dbo::collection<Wt::Dbo::ptr<piece>> get_pieces(Wt::Dbo::Session &, std::string const & title);
    std::string title;
    std::string difficulty;
    std::string composer;
    std::string transpositions;
    std::string edition;
    std::string duration;
    std::string range;
    std::string endurance;
    Wt::Dbo::ptr<struct user> author;

    template<class A>
    void persist(A & a)
    {
        Wt::Dbo::field(a, title, "title");
        Wt::Dbo::field(a, difficulty, "difficulty");
        Wt::Dbo::field(a, composer, "composer");
        Wt::Dbo::field(a, transpositions, "transpositions");
        Wt::Dbo::field(a, edition, "edition");
        Wt::Dbo::field(a, duration, "duration");
        Wt::Dbo::field(a, range, "range");
        Wt::Dbo::field(a, endurance, "endurance");
        Wt::Dbo::belongsTo(a, author, "author");
    }
};
