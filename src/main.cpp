#include "stdafx.hpp"
#include "stopwatch.hpp"
#include "data_model.hpp"
#include "hornburst.hpp"
#include "login_service.hpp"

data_model model{std::make_unique<Wt::Dbo::backend::Sqlite3>("hornburst.data")};
login_service login{model};

auto create_application(Wt::WEnvironment const & env)
{
    return std::unique_ptr<Wt::WApplication>(new hornburst{env, model, login});
}

bool setup_db(void)
{
    try
    {
        model.createTables();
        WARN("Recreated database.");
        return false;
    }
    catch (Wt::Dbo::Exception & e)
    {
        WARN("Database not recreated because of {}. Assuming db already exists.", e.what());
    }
    return true;
}

int main(int argc, char ** argv)
{
    if (!setup_db())
        return 1;
    return Wt::WRun(argc, argv, &create_application);
}
