#include "stdafx.hpp"
#include "data_model.hpp"
#include "user.hpp"
#include "piece.hpp"

data_model::data_model(std::unique_ptr<Wt::Dbo::SqlConnection> && con)
{
    con->setProperty("show-queries", "true");
    setConnection(std::move(con));
    mapClass<user>("user");
    mapClass<piece>("piece");
}

    bool data_model::add_new_piece(std::string const & title, std::string const & difficulty, std::string const & composer,
            std::string const & transpositions, std::string const & edition, std::string const & duration,
            std::string const & range, std::string const & endurance)
{
    Wt::Dbo::Transaction t{*this};
    auto new_piece{addNew<piece>(title, difficulty, composer, transpositions, edition, duration, range, endurance)};
    return new_piece.get();
}

std::vector<piece_ptr> data_model::get_pieces(void)
{
    Wt::Dbo::Transaction t{*this};
    Wt::Dbo::collection<piece_ptr> pieces{find<piece>().resultList()};
    return {pieces.begin(), pieces.end()};
}
