#pragma once

struct add_piece_widget : public Wt::WContainerWidget
{
    add_piece_widget(struct data_model &, struct login_service &);

private:
    data_model & model;
    login_service & login;
    Wt::WLineEdit * title {nullptr};
    Wt::WLineEdit * difficulty {nullptr};
    Wt::WLineEdit * composer {nullptr};
    Wt::WLineEdit * transpositions {nullptr};
    Wt::WLineEdit * edition {nullptr};
    Wt::WLineEdit * duration {nullptr};
    Wt::WLineEdit * range {nullptr};
    Wt::WLineEdit * endurance {nullptr};

    void submit(void);
};
