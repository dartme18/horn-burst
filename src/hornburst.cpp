#include "stdafx.hpp"
#include "utils.hpp"
#include "hornburst.hpp"
#include "data_model.hpp"
#include "pieces_widget.hpp"
#include "piece.hpp"
#include "header_widget.hpp"
#include "home_widget.hpp"
#include "search_widget.hpp"
#include "add_piece_widget.hpp"

hornburst::hornburst(Wt::WEnvironment const & env, data_model & model, login_service & login)
    : Wt::WApplication{env}
    , model{model}
    , login{login}
{
    internalPathChanged().connect(this, &hornburst::prepare_page);
    header = root()->addNew<header_widget>(model, login);
    body = root()->addNew<Wt::WStackedWidget>();
    add_piece = body->addNew<add_piece_widget>(model, login);
    search = body->addNew<search_widget>(model, login);
    home = body->addNew<home_widget>();
    prepare_page();
    search->search_parameters_requested().connect(this, &hornburst::search_parameters_updated);
}

/*
 * Consults internalPath and shows the correct page.
 */
void hornburst::prepare_page(void)
{
    DEBUG("Handling internal path. {}", internalPath());
    if (internalPath() == "/")
        show_home();
    else if (internalPathMatches("/search"))
        show_search();
    else if (internalPathMatches("/add_piece"))
        show_add_piece();
    else
    {
        ERROR("Unknown internal path {}", internalPath());
        show_home();
    }
}

void hornburst::show_home(void)
{
    body->setCurrentWidget(home);
}

void hornburst::show_search(void)
{
    search_parameters parameters;
    std::string navigated = "/search/";
    while (true)
    {
        std::string next_part {internalPathNextPart(navigated)};
        if (!next_part.size())
            break;
        navigated += next_part + "/";
        if (next_part == "sort_by")
        {
            parameters.sort_by = internalPathNextPart(navigated);
            navigated += parameters.sort_by + "/";
        }
    }
    search->set_parameters(parameters);
    body->setCurrentWidget(search);
}

void hornburst::show_add_piece(void)
{
    body->setCurrentWidget(add_piece);
}

void hornburst::search_parameters_updated(search_parameters const & parameters)
{
    INFO("Got search parameters with sort {}", parameters.sort_by);
    if (internalPathNextPart("/") != "search")
    {
        ERROR("Updating search parameters on some page besides search!? aborting.");
        return;
    }
    std::string new_path{"/search/"};
    std::string old_path{"/search/"};
    bool got_sort_by{false};
    while(true)
    {
        std::string next_part{internalPathNextPart(old_path)};
        if (!next_part.size())
            break;
        old_path += next_part + "/";
        new_path += next_part + "/";
        INFO("internal path next part after {}: {}", old_path, next_part);
        if (next_part == "sort_by")
        {
            got_sort_by = true;
            std::string sort {internalPathNextPart(old_path)};
            INFO("old sort path {}.", sort);
            old_path += sort + "/";
            new_path += parameters.sort_by + "/";
        }
        else if (next_part == "criteria")
        {
            INFO("old criteria {}", next_part);
        }
    }
    if (!got_sort_by)
    {
        new_path += "sort_by/" + parameters.sort_by;
    }
    INFO("Setting new internal path to {}.", new_path);
    setInternalPath(new_path, false);
}
