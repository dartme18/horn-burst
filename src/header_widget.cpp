#include "stdafx.hpp"

#include "header_widget.hpp"
#include "utils.hpp"

header_widget::header_widget(struct data_model & model, struct login_service & login)
    : model{model}
    , login{login}
{
    addWidget(get_link("HornBurst", "/"));
    addWidget(get_link("Search", "/search"));
    addWidget(get_link("Add New Piece", "/add_piece"));
}
