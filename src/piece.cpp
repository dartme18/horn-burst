#include "stdafx.hpp"

#include "piece.hpp"

piece::piece(std::string const & title, std::string const & difficulty, std::string const & composer,
            std::string const & transpositions, std::string const & edition,
            std::string const & duration, std::string const & range,
            std::string const & endurance)
    : title{title}
    , difficulty{difficulty}
    , composer{composer}
    , transpositions{transpositions}
    , edition{edition}
    , duration{duration}
    , range{range}
    , endurance{endurance}
{

}
