#include "stdafx.hpp"
#include "utils.hpp"

std::unique_ptr<Wt::WContainerWidget> get_link(std::string const & text, std::string const & target)
{
    Wt::WContainerWidget * logo_container = new Wt::WContainerWidget();
    Wt::WAnchor * logo_link = logo_container->addNew<Wt::WAnchor>();
    logo_link->setLink(Wt::WLink{Wt::LinkType::InternalPath, target});
    logo_link->setText(text);
    return std::unique_ptr<Wt::WContainerWidget>{logo_container};
}
