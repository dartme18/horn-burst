#ifndef STOPWATCH_HEADER
#define STOPWATCH_HEADER
#include "stdafx.hpp"

struct stopwatch
{
    using tp = std::chrono::time_point<std::chrono::high_resolution_clock>;

    /*
     * Saves a time according to the given stopwatch to be compared
     * later with get_difference.
     */
    void save_time(std::string const &) noexcept;

    /*
     * Returns the amount of time since calling save_time with the
     * given stopwatch.
     */
    double get_ms(std::string const &) noexcept;

private:
    std::map<std::string const, tp> times;
};

#endif
