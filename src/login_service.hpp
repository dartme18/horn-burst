#pragma once

#include "user.hpp"

struct login_service
{
    login_service(struct data_model &);
    bool is_logged_in(void) const;
    user_ptr get_logged_in_user(void) const;
    Wt::Signal<> & changed(void);
    void log_out(void);

private:
    data_model & model;
};
