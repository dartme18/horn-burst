#pragma once

#include "Wt/WApplication.h"

struct hornburst : public Wt::WApplication
{
    hornburst(Wt::WEnvironment const &, struct data_model &, struct login_service &);

private:
    data_model & model;
    login_service & login;
    struct create_entry_widget * create_entry {nullptr};
    struct header_widget * header {nullptr};
    struct add_piece_widget * add_piece {nullptr};
    struct home_widget * home {nullptr};
    struct search_widget * search {nullptr};
    Wt::WStackedWidget * body {nullptr};

    void show_home(void);
    void show_search(void);
    void show_add_piece(void);
    void prepare_page(void);
    void search_parameters_updated(struct search_parameters const &);
};
