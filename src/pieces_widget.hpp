#pragma once
#include "utils.hpp"

// Shows a listing of pieces
struct pieces_widget : public Wt::WContainerWidget
{
    void show_pieces(std::vector<Wt::Dbo::ptr<struct piece>> && pieces, search_parameters const & = {});
    void set_parameters(search_parameters const &);
    Wt::Signal<search_parameters const &> & search_parameters_requested(void);

private:
    Wt::Signal<search_parameters const &> _search_parameters_requested;
    std::vector<Wt::Dbo::ptr<struct piece>> pieces;
    search_parameters parameters;
    Wt::WTable * table;
    int showing_details{-1};

    void add_table_header(Wt::WTable &, std::string const &);
    void sort_by(std::string const &);
    void render(void);
    void add_element(Wt::WTable &, int, int, std::string const &, Wt::Dbo::ptr<struct piece> const &);
    void toggle_detail(int);
};
