#include "stopwatch.hpp"

void stopwatch::save_time(std::string const & name) noexcept
{
    DEBUG("saving time...");
    /*insert or replace!*/
    times[name] = std::chrono::high_resolution_clock::now();
    DEBUG("Saved.");
}

double stopwatch::get_ms(std::string const & name) noexcept
{
    auto const spot {times.find(name)};
    if (spot == times.end())
        return {};
    std::chrono::duration<double,std::milli> const ret {std::chrono::high_resolution_clock::now() - spot->second};
    return ret.count();
}


