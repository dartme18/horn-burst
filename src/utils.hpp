#pragma once

struct search_parameters
{
    std::vector<std::pair<std::string, std::string>> search_criteria;
    std::string sort_by;
};

std::unique_ptr<Wt::WContainerWidget> get_link(std::string const & text, std::string const & target);
