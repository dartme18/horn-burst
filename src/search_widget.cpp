#include "stdafx.hpp"

#include "search_widget.hpp"
#include "pieces_widget.hpp"
#include "data_model.hpp"

search_widget::search_widget(struct data_model & model, struct login_service & login)
    : model{model}
    , login{login}
{
    //auto title_label{this->addNew<Wt::WLabel>("Title:")};
    //title = this->addNew<Wt::WLineEdit>();
    //title_label->setBuddy(title);
    //auto difficulty_label{this->addNew<Wt::WLabel>("Difficulty:")};
    //difficulty = this->addNew<Wt::WLineEdit>();
    //difficulty_label->setBuddy(difficulty);
    //auto composer_label{this->addNew<Wt::WLabel>("Composer:")};
    //composer = this->addNew<Wt::WLineEdit>();
    //composer_label->setBuddy(composer);
    //auto transpositions_label{this->addNew<Wt::WLabel>("Transpositions:")};
    //transpositions = this->addNew<Wt::WLineEdit>();
    //transpositions_label->setBuddy(transpositions);
    //auto edition_label{this->addNew<Wt::WLabel>("Edition:")};
    //edition = this->addNew<Wt::WLineEdit>();
    //edition_label->setBuddy(edition);
    //auto duration_label{this->addNew<Wt::WLabel>("Duration:")};
    //duration = this->addNew<Wt::WLineEdit>();
    //duration_label->setBuddy(duration);
    //auto range_label{this->addNew<Wt::WLabel>("Range:")};
    //range = this->addNew<Wt::WLineEdit>();
    //range_label->setBuddy(range);
    //auto endurance_label{this->addNew<Wt::WLabel>("Endurance:")};
    //endurance = this->addNew<Wt::WLineEdit>();
    //endurance_label->setBuddy(endurance);
    auto search{addNew<Wt::WPushButton>("Search")};
    search->clicked().connect(this, &search_widget::search);
    results = addNew<pieces_widget>();
}

void search_widget::search(void)
{
    results->show_pieces(model.get_pieces());
}

Wt::Signal<search_parameters const &> & search_widget::search_parameters_requested(void)
{
    return results->search_parameters_requested();
}

void search_widget::set_parameters(search_parameters const & parameters)
{
    results->set_parameters(parameters);
    search();
}
