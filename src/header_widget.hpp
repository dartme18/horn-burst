#pragma once

struct header_widget : public Wt::WContainerWidget
{
    header_widget(struct data_model &, struct login_service &);

private:
    data_model & model;
    login_service & login;
};
