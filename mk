#!/bin/bash
if [[ -e making ]]; then
    echo "Make in progress, not making" >&2;
    exit 1;
fi;
touch making;
make -j $(grep processor /proc/cpuinfo | wc -l) "$@";
ret=$?;
rm making;
exit "$ret";
